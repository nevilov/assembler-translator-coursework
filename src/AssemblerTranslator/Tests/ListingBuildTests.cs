using System.Collections.Generic;
using AssemblerTranslator;
using Xunit;

namespace Tests;

public class ListingBuildTests
{
    [Fact]
    public void BuildCode_For_Command_Mov()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, 10; Комментарий!!!",
            "mov bx, 5",
            "mov bx, ax;",
        }.ToArray();
        
        //Act
        var parser = new Parser();
        parser.Process(input);
        var builder = new CodeBuilder(parser.AssemblerCommands.ToArray(), parser.ErrorsList, parser.AssemblerMetas.ToArray(), parser.Lines);
        var values = builder.Build();

        //Assert
        Assert.NotEmpty(values.BuilderResponses);
        Assert.Collection(values.BuilderResponses, 
            x => Assert.Equal("101110000000101000000000", x.BinaryMachineCode.ToUpper()),
            x => Assert.Equal("BB 05 00", x.FormatBinaryMachineCode.ToUpper()),
            x => Assert.Equal("8BD8", x.MachineCode.ToUpper())
        );
        
    }
    
    [Fact]
    public void BuildCode_With_Segment()
    {
        //Arrange
        var input = new List<string>()
        {
            "v segment",
            "assume , k DS    njjg",
            "org 100h",
            "mov ax, 5"
        }.ToArray();
        
        //Act
        var parser = new Parser();
        parser.Process(input);
        var builder = new CodeBuilder(parser.AssemblerCommands.ToArray(), 
            parser.ErrorsList,
            parser.AssemblerMetas.ToArray(),
            parser.Lines);
        var values = builder.Build();

        //Assert
        Assert.NotEmpty(values.BuilderResponses);
        Assert.Collection(values.BuilderResponses,
            x => Assert.Equal("B80500", x.MachineCode.ToUpper())
        );
    }

    [Fact]
    public void Build_For_Command_Mov_With_Variable()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, var1",
            "mov var2, ax",
            "mov si, ax;",
            "var1 dw 5",
            "var2 dw 10"
        }.ToArray();
        var inputString = string.Join("\n", input);
        
        //Act
        var result = new Runner(RunnerMode.SourceCode).Run(inputString);

        //Assert
        Assert.NotEmpty(result.BuilderResponses);
        Assert.Collection(result.BuilderResponses, 
            x => Assert.Equal("A10801", x.MachineCode.ToUpper()),
            x => Assert.Equal("A30A01", x.MachineCode.ToUpper()),
            x => Assert.Equal("8BF0", x.MachineCode.ToUpper()),
            x => Assert.Equal("0500", x.MachineCode.ToUpper()),
            x => Assert.Equal("0A00", x.MachineCode.ToUpper())
        );
    }

    [Fact]
    public void Build_ForCommand_Not_With_Variable()
    {
        //Arrange
        var input = new List<string>()
        {
            "not var1",
            "not var2",
            "var1 dw 10",
            "var2 dw 15"
        }.ToArray();
        var inputString = string.Join("\n", input);
        
        //Act
        var result = new Runner(RunnerMode.SourceCode).Run(inputString);

        //Assert
        Assert.NotEmpty(result.BuilderResponses);
        Assert.Collection(result.BuilderResponses, 
            x => Assert.Equal("F7160801", x.MachineCode.ToUpper()),
            x => Assert.Equal("F7160A01", x.MachineCode.ToUpper()),
            x => Assert.Equal("0A00", x.MachineCode.ToUpper()),
            x => Assert.Equal("0F00", x.MachineCode.ToUpper())
        );
    }  
    
    [Fact]
    public void BuildCode_For_Command_Mov_With_Indirect_Operand()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, [bx]",
            "mov si, ax;",
            "mov [bx], si",
            "mov [bx], ax"
        }.ToArray();
        
        //Act
        var parser = new Parser();
        parser.Process(input);
        var builder = new CodeBuilder(parser.AssemblerCommands.ToArray(), parser.ErrorsList, parser.AssemblerMetas.ToArray(), parser.Lines);
        var values = builder.Build();

        //Assert
        Assert.NotEmpty(values.BuilderResponses);
        Assert.Collection(values.BuilderResponses, 
            x => Assert.Equal("8B07", x.MachineCode.ToUpper()),
            x => Assert.Equal("8BF0", x.MachineCode.ToUpper()),
            x => Assert.Equal("8937", x.MachineCode.ToUpper()),
            x => Assert.Equal("8907", x.MachineCode.ToUpper())
            
        );
    }
    
    [Fact]
    public void BuildCode_For_Command_Not()
    {
        //Arrange
        var input = new List<string>()
        {
            "not ax",
            "not [bx]",
            "not bp",
        }.ToArray();
        
        //Act
        var parser = new Parser();
        parser.Process(input);
        var builder = new CodeBuilder(parser.AssemblerCommands.ToArray(), parser.ErrorsList, parser.AssemblerMetas.ToArray(),parser.Lines);
        var values = builder.Build();

        //Assert
        Assert.NotEmpty(values.BuilderResponses);
        Assert.Collection(values.BuilderResponses, 
            x => Assert.Equal("F7D0", x.MachineCode.ToUpper()),
            x => Assert.Equal("F617", x.MachineCode.ToUpper()),
            x => Assert.Equal("F7D5", x.MachineCode.ToUpper())
        );
    }

}