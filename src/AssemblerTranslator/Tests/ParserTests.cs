using System.Collections.Generic;
using AssemblerTranslator;
using AssemblerTranslator.AssemblerCommands;
using AssemblerTranslator.AssemblerCommands.Commands;
using AssemblerTranslator.Constants.CommandFormat;
using AssemblerTranslator.Types;
using Xunit;

namespace Tests;

public class ParserTests
{
    [Fact]
    public void Parse_MovCommand_WithRegisterImmidiateFormat()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, 5;",
            "mov bx, 6;"
        }.ToArray();
       
        //Act
        Parser parser = new Parser();
        parser.Process(input);
        
        //Assert
        var values = parser.AssemblerCommands;
        Assert.NotEmpty(values);
        Assert.All(values, x => Assert.True(x is CommandMov));
        Assert.All(values, x =>
        {
            var command = (CommandMov)x;
            Assert.True(command.Format is CommandFormat.RegisterImmediate);
        });
    }

    [Fact]
    public void Parse_MovCommand_WithDifferentFormats()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, 5;",
            "mov bx, ax;",
            "mov bx, N",
            "mov ax, [n];"
        }.ToArray();
       
        //Act
        Parser parser = new Parser();
        parser.Process(input);
        
        //Assert
        var values = parser.AssemblerCommands;
        Assert.NotEmpty(values);
        Assert.All(values, x => Assert.True(x is CommandMov));
        Assert.Collection(values, 
            x => Assert.Equal(CommandFormat.RegisterImmediate, ((x as CommandMov)!).Format),
            x => Assert.Equal(CommandFormat.RegisterRegister, ((x as CommandMov)!).Format),
            x => Assert.Equal(CommandFormat.RegisterMemory,((x as CommandMov)!).Format),
            x => Assert.Equal(CommandFormat.AxRegisterMemory,((x as CommandMov)!).Format)
        );
    }

    [Fact]
    public void Parse_Command_With_Label()
    {
        //Arrange
        var input = new List<string>()
        {
            "mov ax, 5;",
            "mov bx, 6;",
            "label: ",
            "mov bx, ax;"
        }.ToArray();
       
        //Act
        Parser parser = new Parser();
        parser.Process(input);
        
        //Assert
    }
}