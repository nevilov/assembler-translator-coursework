﻿using AssemblerTranslator.Extensions;
using Xunit;

namespace Tests;

public class ConverterTests
{
    [Fact]
    public void Convert_From_Decimal_To_Binary()
    {
        var number = 256;
        var binary = number.ConvertTo(2);
        
        Assert.Equal("100000000", binary);
    }
    
    [Fact]
    public void Convert_From_Binary_To_Decimal()
    {
        var binary = "100000000";
        var number = binary.ConvertFrom(2);
        
        Assert.Equal(256, number);
    }
}