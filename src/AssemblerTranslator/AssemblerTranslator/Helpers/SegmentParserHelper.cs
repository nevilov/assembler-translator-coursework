﻿using System.Globalization;
using AssemblerTranslator.AssemblerCommands;
using AssemblerTranslator.AssemblerMeta;
using AssemblerTranslator.Extensions;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.Helpers;

public class SegmentParserHelper
{
    private readonly List<string> ErrorsList = new List<string>();
    public (AssemblerSegment, IEnumerable<string>) Parse(string[] lines)
    {
        var potentialSegment = lines
            .SkipWhile(x => !x.Contains("segment"))
            .TakeWhileCustom(x => !x.Contains("org", StringComparison.InvariantCultureIgnoreCase), true)
            .ToList();

        var segmentName = string.Empty;
        var startAddress = 256;
        foreach (var line in potentialSegment)
        {
            var formattedLine = line
                .Split(' ')
                .Select(x => x.Trim())
                .Where(x => !string.IsNullOrEmpty(x))
                .ToArray();
            
            if (formattedLine.Contains("segment"))
            {
                segmentName = formattedLine[0];
            }

            if (formattedLine.Contains("org"))
            {
                var potentialAddress = formattedLine
                    .First(x => !x.Equals("org", StringComparison.InvariantCultureIgnoreCase))
                    .Trim()
                    .TrimEnd('h');
                
                if (!int.TryParse(potentialAddress, NumberStyles.HexNumber, null, out var result))
                {
                    ErrorsList.Add("Неправильное опрделение сегмента! " + line);
                }

                startAddress = result;
            }
        }

        var command = new AssemblerSegment(startAddress, segmentName, potentialSegment.ToArray());
        return (command, ErrorsList);
    }
    
}