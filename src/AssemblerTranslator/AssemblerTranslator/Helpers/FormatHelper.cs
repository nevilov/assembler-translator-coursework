﻿using System.Globalization;
using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.Constants;
using AssemblerTranslator.Constants.CommandFormat;
using AssemblerTranslator.Exceptions;

namespace AssemblerTranslator.Helpers;

public class FormatHelper
{
    public static FormatResponse GetFormat(string f, string s)
    {
        var firstOperand = DetectOperandType(f);
        var secondOperand = DetectOperandType(s);

        var response = new FormatResponse()
        {
            FirstOperand = firstOperand,
            SecondOperand = secondOperand,
        };

        if (firstOperand is RegisterOperand && secondOperand is RegisterOperand)
        {
            response.CommandFormat = CommandFormat.RegisterRegister;
        }
        else if (firstOperand is RegisterOperand && secondOperand is DirectOperand)
        {
            response.CommandFormat = CommandFormat.RegisterImmediate;
        }
        else if (firstOperand is RegisterOperand || secondOperand is RegisterOperand)
        {
            if (firstOperand is not RegisterOperand)
            {
                response.CommandFormat = ((RegisterOperand)secondOperand).IsAx
                    ? CommandFormat.MemoryAxRegister 
                    : CommandFormat.MemoryRegister;
            }
            else
            {
                response.CommandFormat = ((RegisterOperand)firstOperand).IsAx
                    ? CommandFormat.AxRegisterMemory
                    : CommandFormat.RegisterMemory;
            }
        }

        if (firstOperand is MemoryOperand && secondOperand is DirectOperand)
        {
        }

        return response;
    }

    public static IOperand DetectOperandType(string operand)
    {
        var registers = PredefinedData.RegisterCodes.Keys.Select(x => x.ToLower()).ToList();
        operand = operand.Trim().ToLower();

        if (registers.Any(x => operand.Contains(x)))
        {
            return DetectAddressCode(operand);
        }
        else if (int.TryParse(operand, out var result))
        {
            if (result > PredefinedData.MaxNumber || result < -PredefinedData.MaxNumber)
            {
                throw new AssemblerVariableOutOfRange(operand);
            }
            var hexValue = result.ToString("X");
            return new DirectOperand(operand, result, hexValue);
        }
        else
        {
            return new MemoryOperand(operand);
        }
    }

    private static IOperand DetectAddressCode(string operand)
    {
        if (operand.Contains('['))
        {
            var operands = operand
                .Split('+', '-').Select(x => x.ToLower().Trim().ToString())
                .ToList();

            var sum = operands
                .Where(x => int.TryParse(x, NumberStyles.Any, new NumberFormatInfo(), out var _))
                .Select(x => int.Parse(x, NumberStyles.Any, new NumberFormatInfo()))
                .Sum();
            var operandsExceptNumbers = operands
                .Where(x => !int.TryParse(x, NumberStyles.Any, new NumberFormatInfo(), out var _));
            
            if (sum != 0 || operandsExceptNumbers.Count() > 1)
            {
                throw new UnsupportedAddressFormatTypeException($"{operand}");
            }

            if (!PredefinedData.AddressCodeDictionary.TryGetValue(operand, out var value))
            {
                throw new InvalidOperationException("Невозможно обратится к данному регистру косвенно");
            }

            return new RegisterOperand(operand, mod: value.Item1, mem: value.Item2, true);
        }

        return new RegisterOperand(operand, mod: "11", mem: "000", false);
    }
    
    public class FormatResponse
    {
        public CommandFormat CommandFormat { get; set; }
        public IOperand FirstOperand { get; set; }
        public IOperand SecondOperand { get; set; }
    }

}