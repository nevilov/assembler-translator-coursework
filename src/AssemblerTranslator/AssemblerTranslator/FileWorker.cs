﻿namespace AssemblerTranslator;

public class FileWorker
{
    public const string FileName = "output.txt";
    public string[] GetLines(string fileName)
    {
        try
        {
            using var stream = File.Open(fileName, FileMode.Open);
            using var reader = new StreamReader(stream);
            List<string> lines = new List<string>();

            while (reader.Peek() >= 0)
            {
                lines.Add(reader.ReadLine() ?? "");
            }

            return lines.ToArray();
        }
        catch (Exception e)
        {
            throw new Exception("Не удалось открыть файл, ошибка!");
        }
    }
    
    public void WriteToFile(string content, string fileName = FileName)
    {
        try
        {
            using var stream = new MemoryStream();
            using var writer = new StreamWriter(stream);
            writer.Write(content);
            writer.Flush();
            stream.Position = 0;
            
            using var fileStream = new FileStream(fileName, FileMode.Truncate, FileAccess.Write);
            stream.CopyTo(fileStream);
        }
        catch(Exception ex)
        {
            throw new Exception("Не удалось открыть файл, ошибка!");
        }
    }
}