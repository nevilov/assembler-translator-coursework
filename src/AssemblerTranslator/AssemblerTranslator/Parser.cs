﻿using System.Globalization;
using AssemblerTranslator.AssemblerCommands;
using AssemblerTranslator.AssemblerCommands.Commands;
using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.AssemblerMeta;
using AssemblerTranslator.Constants;
using AssemblerTranslator.Exceptions;
using AssemblerTranslator.Extensions;
using AssemblerTranslator.Helpers;
using AssemblerTranslator.Types;

namespace AssemblerTranslator;

public class Parser
{
    private readonly FileWorker _fileWorker;
    private readonly SegmentParserHelper _segmentParserHelper;

    public string[] Lines { get; set; }
    
    public readonly List<IAssemblerCommand> AssemblerCommands = new();
    public readonly List<IAssemblerMeta> AssemblerMetas = new();
    public readonly List<string> ErrorsList = new();
    
    public Parser()
    {
        _fileWorker = new FileWorker();
        _segmentParserHelper = new SegmentParserHelper();
    }
    
    public void Process(string[] lines)
    {
        Lines = lines;
        Parse();
    }
    
    public void Process(string fileName)
    {
        Lines = _fileWorker.GetLines(fileName);
        Parse();
    }

    private void Parse()
    {
        var (segmentParseResult, errors) = _segmentParserHelper.Parse(Lines);
        AssemblerMetas.Add(segmentParseResult);
        ErrorsList.AddRange(errors);

        foreach (var line in Lines.Where(x => !segmentParseResult.InitialLines.Contains(x)))
        {
            var words = line.SplitOnTokens();
            ParseBody(words, line);
        }
    }

    private void ParseBody(string[] words, string initialLine)
    {
        IAssemblerCommand assemblerCommand = null;
        foreach (var word in words)
        {
            switch (word)
            {
                case var label when label.Contains(':'):
                    assemblerCommand = ParseLabel(label);
                    break;
                case var command when PredefinedData.CommandNames.Contains(command, StringComparer.InvariantCultureIgnoreCase): 
                    assemblerCommand = ParseCommand(command, words, initialLine);
                    break;
                case var variable when PredefinedData.VariableTypes.Contains(variable, StringComparer.InvariantCultureIgnoreCase):
                    assemblerCommand = ParseVariable(variable, words, initialLine);
                    break;
            }
 
            if (assemblerCommand != null)
            {
                assemblerCommand.InitialWord = initialLine;
                AssemblerCommands.Add(assemblerCommand);

                if (assemblerCommand is not AssemblerLabel)
                {
                    break;
                }
            }
        }

        if (assemblerCommand is null)
        {
            ErrorsList.Add($"Невозможно распознать команду {initialLine}");

        }
    }

    private IAssemblerCommand ParseVariable(string command, string[] words, string initialLine)
    {
        var variableName = words[0];
        if (words.Length != 3)
        {
            throw new InvalidAssemblerCommand(command, "Неверное определение переменной", initialLine);
        }

        var potentialVariableValue = words[2];
        int variable = default;
        if (potentialVariableValue.Contains("h"))
        {
            potentialVariableValue = potentialVariableValue.Replace("h", string.Empty);
            if (!int.TryParse(potentialVariableValue, NumberStyles.HexNumber, null, out variable))
            {
                throw new InvalidAssemblerCommand(command, "Неверное определение переменной", initialLine);
            }
        }
        else
        {
            if (!int.TryParse(potentialVariableValue, out variable))
            {
                throw new InvalidAssemblerCommand(command, "Неверное определение переменной", initialLine);
            }
        }

        if (variable > PredefinedData.MaxNumber || variable < -PredefinedData.MaxNumber)
        {
            throw new AssemblerVariableOutOfRange(initialLine);
        }

        var originalValue = variable.ConvertTo(16);
        
        return new AssemblerVariable(variableName, variable);
    }

    private IAssemblerCommand ParseLabel(string label)
    {
        label = label.TrimEnd(':').Trim();
        return new AssemblerLabel(label, Keywords.Label);
    }
    
    private IAssemblerCommand ParseCommand(string command, string[] words, string initialLine)
    {
        var args = words.Where(word => word != command).ToArray();
        command = command.Trim().ToLower();

        return command switch
        {
            "mov" => ParseMov(command, args, initialLine),
            "not" => ParseNot(command, args, initialLine),
            "loop" => ParseLoop(command, args, initialLine),
            "sbb" => ParseSbb(command, args, initialLine),
            _ => null!
        };
    }

    private IAssemblerCommand ParseSbb(string command, string[] arguments, string initialLine)
    {
        if (arguments.Length != 2)
        {
            throw new InvalidAssemblerCommand(command, "Аргументов больше или меньше 2!", initialLine);
        }

        var format = FormatHelper.GetFormat(arguments[0], arguments[1]);
        var detectedArgs = new IOperand[] { format.FirstOperand, format.SecondOperand };

        return new CommandSbb(detectedArgs, format.CommandFormat);
    }

    private IAssemblerCommand ParseLoop(string command, string[] args, string initialLine)
    {
        if (args.Length != 1)
        {
            throw new InvalidAssemblerCommand(command, "Аргументов больше 1!", initialLine);
        }
        var labelName = args[0].Trim();
        return new CommandLoop(labelName);
    }

    private IAssemblerCommand ParseMov(string command, string[] arguments, string initialLine)
    {
        if (arguments.Length != 2)
        {
            throw new InvalidAssemblerCommand(command, "Аргументов больше или меньше 2!", initialLine);
        }

        var format = FormatHelper.GetFormat(arguments[0], arguments[1]);
        var detectedArgs = new IOperand[] { format.FirstOperand, format.SecondOperand };

        return new CommandMov(detectedArgs, format.CommandFormat);
    }

    private IAssemblerCommand ParseNot(string command, string[] arguments, string initialLine)
    {
        if (arguments.Length != 1)
        {
            throw new InvalidAssemblerCommand(command, "Аргумент должен быть один!", initialLine);
        }
        var potentialOperand = arguments[0].Trim();
        var operands = new IOperand[] { FormatHelper.DetectOperandType(potentialOperand) };

        if (operands[0] is DirectOperand)
        {
            throw new InvalidAssemblerCommand(command, "Не поддерживает непосредственный операнд!", initialLine);
        }
        
        return new CommandNot(operands);
    }
    
}