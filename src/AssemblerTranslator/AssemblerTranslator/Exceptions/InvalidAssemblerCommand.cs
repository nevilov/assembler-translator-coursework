namespace AssemblerTranslator.Exceptions;

public class InvalidAssemblerCommand : Exception
{
    public InvalidAssemblerCommand(string commandName, string message, string initialLine) : base($"Неверная ассемблер команда {commandName}. {message}: {initialLine}")
    {
    }
}