using AssemblerTranslator.Constants;

namespace AssemblerTranslator.Exceptions;

public class AssemblerVariableOutOfRange : Exception
{
    public AssemblerVariableOutOfRange(string initialLine) : base($"Нарушение допустимого диапазона, предел {PredefinedData.MaxNumber}.{initialLine}")
    {
        
    }
}