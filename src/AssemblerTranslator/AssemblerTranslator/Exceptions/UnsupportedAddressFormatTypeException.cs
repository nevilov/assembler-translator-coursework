﻿namespace AssemblerTranslator.Exceptions;

public class UnsupportedAddressFormatTypeException : AssemblerTranslatorException
{
    public UnsupportedAddressFormatTypeException(string message) : base(
        "Неподдержимый формат адреса: " + message)
    {
    }
}