﻿namespace AssemblerTranslator.Exceptions;

public class AssemblerTranslatorException: Exception
{
    public AssemblerTranslatorException(string message) : base(message)
    {
        
    }
}