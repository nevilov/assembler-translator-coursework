﻿namespace AssemblerTranslator.Types;

public enum Keywords
{
    Label,
    Variable,
    Command,
    Segment
}