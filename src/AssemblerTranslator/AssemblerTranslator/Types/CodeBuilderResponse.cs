﻿using AssemblerTranslator.AssemblerCommands.Types;

namespace AssemblerTranslator.Types;

public class CodeBuilderResponse
{
    public IEnumerable<BuildResponse> BuilderResponses { get; set; }
    public string Listing { get; set; }
}