﻿namespace AssemblerTranslator.Constants.CommandFormat;

// MOV AX, BX

public enum CommandFormat
{
    RegisterRegister,
    RegisterMemory, //Из регистра в память
    MemoryRegister,
    //MemoryImmediate
    RegisterImmediate,
    MemoryAxRegister,
    AxRegisterMemory,
    AxRegisterImmediate
} 