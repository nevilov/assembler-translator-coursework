﻿namespace AssemblerTranslator.Constants.CommandFormat;

public static class Formats
{
    public class FormatMeta
    {
        public int Offset { get; set; }
        public string Code { get; set; }

        public FormatMeta(string code, int offset)
        {
            Offset = offset;
            Code = code;
        }
    }
    
    public static Dictionary<CommandFormat, FormatMeta> CommandFormatForMov = new()
    {
        // КУДА <- ИЗ AX, 5
        {CommandFormat.MemoryRegister, new FormatMeta("100010", 4)}, //Из регистра в память
        {CommandFormat.RegisterRegister,new FormatMeta("100010", 2)},
        {CommandFormat.RegisterMemory, new FormatMeta("100010", 4)},
        {CommandFormat.MemoryAxRegister, new FormatMeta("1010001", 3)},
        {CommandFormat.AxRegisterMemory, new FormatMeta("1010000", 3)},
        {CommandFormat.RegisterImmediate, new FormatMeta("1011", 3)}, //RegAxIm
        {CommandFormat.AxRegisterImmediate, new FormatMeta("1011", 3)} //RegAxIm
    };

    public static Dictionary<CommandFormat, FormatMeta> CommandFormatForSbb = new()
    {
        { CommandFormat.RegisterImmediate, new FormatMeta("100000", 3) }, //Из регистра в память
        { CommandFormat.AxRegisterMemory, new FormatMeta("000110", 3) }, //Из регистра в память
        { CommandFormat.RegisterRegister, new FormatMeta("000110", 2) }, //Из регистра в память
        { CommandFormat.MemoryRegister, new FormatMeta("000110", 4) }, //Из регистра в память
        { CommandFormat.MemoryAxRegister, new FormatMeta("000110", 3) },
        { CommandFormat.AxRegisterImmediate, new FormatMeta("0001110", 3)},
    };

}