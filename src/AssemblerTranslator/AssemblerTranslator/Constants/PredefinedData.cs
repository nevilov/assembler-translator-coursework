﻿namespace AssemblerTranslator.Constants;

public class PredefinedData
{
    public static readonly int MaxNumber = 65536;
    
    public static IEnumerable<string> SegmentKeyWords = new List<string>()
    {
        "segment", "assume",
    };

    public static Dictionary<string, string> RegisterCodes = new Dictionary<string, string>()
    {
        { "AX", "000" },
        { "CX", "001" },
        { "DX", "010" },
        { "BX", "011" },
        { "SP", "100" },
        { "BP", "101" },
        { "SI", "110" },
        { "DI", "111" }
    };

    public static readonly string[] CommandNames = new[]
    {
        "mov", "not", "loop", "sbb"
    };

    public static readonly string[] VariableTypes = new[]
    {
        "dw"
    };

    public static IDictionary<string, (string, string)> AddressCodeDictionary = new Dictionary<string, (string, string)>()
    {
        {"[si]", ("00", "100")},
        {"[di]", ("00", "101")},
        {"[bx]", ("00", "111")},
        {"[bp]", ("01", "111")}, // Нулевое смещение
    };
}