﻿namespace AssemblerTranslator;

/// <summary>
/// MOV AX,5 => MOV;AX;5
/// Комментарии ;ContentWorker
/// </summary>

public class Program
{
    public static void Main()
    {
        var result = new Runner(RunnerMode.File)
            .Run("file.txt");
    }
}