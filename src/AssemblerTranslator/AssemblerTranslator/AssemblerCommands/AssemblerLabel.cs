using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.AssemblerCommands;

public class AssemblerLabel : IAssemblerCommand
{
    public string Content { get; set; }
    
    public AssemblerLabel(string content, Keywords keyword) : base(keyword)
    {
        Content = content;
    }

    public override BuildResponse Build()
    {
        return new BuildResponse(0, string.Empty, string.Empty);
    }

    public override int GetOffset()
    {
        return 0;
    }
}