using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.AssemblerCommands;

public abstract  class AssemblerCommand : IAssemblerCommand
{
    public IOperand[] Arguments { get; set; }

    public AssemblerCommand(IOperand[] args) : base(Keywords.Command)
    {
        Arguments = args;
    }
}
