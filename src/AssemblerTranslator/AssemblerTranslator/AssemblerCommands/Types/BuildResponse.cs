namespace AssemblerTranslator.AssemblerCommands.Types;

public class BuildResponse
{
    public BuildResponse(int offset, string machineCode, string binaryMachineCode)
    {
        Offset = offset;
        MachineCode = machineCode;
        BinaryMachineCode = binaryMachineCode;
    }

    public int Offset { get; set; }

    public string MachineCode { get; set; }

    public string BinaryMachineCode { get; set; }

    public string FormatBinaryMachineCode =>
        string.Join(string.Empty, MachineCode.Chunk(2).Select(x => string.Join(string.Empty, x) + " ")).Trim();
}