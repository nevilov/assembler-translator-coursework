using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Extensions;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.AssemblerCommands;

public class AssemblerVariable : IAssemblerCommand
{
    public string VariableName { get; set; }
    public int Value { get; set; }
    public int OriginalValue { get; set; }
    
    
    public AssemblerVariable(string variableName, int value) : base(Keywords.Variable)
    {
        VariableName = variableName;
        Value = value;
    }

    public override BuildResponse Build()
    {
        var binary = Value.ConvertTo(2)
            .ExtendTo(16)
            .ReverseTwoBytes();

        return new BuildResponse(GetOffset(), binary.ConvertTo(2, 16).ExtendTo(4), binary);
    }

    public override int GetOffset()
    {
        return 2;
    }
}