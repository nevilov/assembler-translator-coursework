namespace AssemblerTranslator.AssemblerCommands.Operators;

public abstract class IOperand
{
    public string Value { get; set; }

    public IOperand(string value)
    {
        Value = value;
    }
}