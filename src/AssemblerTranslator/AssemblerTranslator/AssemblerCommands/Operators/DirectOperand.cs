﻿namespace AssemblerTranslator.AssemblerCommands.Operators;

public class DirectOperand : IOperand
{
    public decimal DirectValue { get; set; }
    public string HexValue { get; set; }
    
    public DirectOperand(string value, decimal directValue, string hexValue) : base(value)
    {
        DirectValue = directValue;
        HexValue = hexValue;
    }
}