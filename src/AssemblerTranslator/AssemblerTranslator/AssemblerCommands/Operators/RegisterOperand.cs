﻿namespace AssemblerTranslator.AssemblerCommands.Operators;

public class RegisterOperand : IOperand
{
    public bool IsAx { get; set; }
    
    public string Mod { get; set; }
    public string Mem { get; set; }
    public int Direction { get; set; }
    public bool IsCustom { get; set; }

    public RegisterOperand(string value, string mod, string mem, bool isCustom) : base(value)
    {
        IsAx = value.Trim().ToLower() == "ax";
        Mem = mem;
        Mod = mod;
        IsCustom = isCustom;
    }
}