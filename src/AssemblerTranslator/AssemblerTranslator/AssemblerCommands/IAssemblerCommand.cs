using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.AssemblerCommands;

public abstract class IAssemblerCommand
{
    public int? CurrentAddress { get; set; } = null!;
    public AssemblerTable[] AssemblerTable { get; set; } = null!;
    public Keywords AssemblerType { get; set; }
    public string InitialWord { get; set; }

    public IAssemblerCommand(Keywords keyword)
    {
        AssemblerType = keyword;
    }
    
    public abstract BuildResponse Build();

    public abstract int GetOffset();
}