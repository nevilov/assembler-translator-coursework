﻿using AssemblerTranslator.Extensions;

namespace AssemblerTranslator.AssemblerCommands;

public class AssemblerTable
{
    public int Address { get; set; }
    public string FormatAddress => Address
        .ConvertTo(2)
        .ExtendTo(16)
        .ReverseTwoBytes()
        .ConvertTo(2, 16);

    public string BinaryAddress => Address.ConvertTo(16).ConvertTo(16, 2).ExtendTo(16);
    public IAssemblerCommand Command { get; set; }

    public AssemblerTable(int address, IAssemblerCommand command)
    {
        Address = address;
        Command = command;
    }
}