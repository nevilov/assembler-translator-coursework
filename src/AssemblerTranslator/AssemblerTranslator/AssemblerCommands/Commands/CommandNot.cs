﻿using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Constants;
using AssemblerTranslator.Extensions;

namespace AssemblerTranslator.AssemblerCommands.Commands;

public class CommandNot : AssemblerCommand
{
    
    public CommandNot(IOperand[] args) : base(args)
    {
    }

    public override BuildResponse Build()
    {
        string w = "1";
        
        var code = "1111011";
        var mod = string.Empty;
        var mem = string.Empty;
        var additions = string.Empty;
        var argument = Arguments[0];

        switch (argument)
        {
            case RegisterOperand operand:
                mod += operand.IsCustom ? operand.Mod : 11;
                mem += operand.IsCustom ? operand.Mem : PredefinedData.RegisterCodes[operand.Value.ToUpper()];
                w = operand.IsCustom ? "0" : "1";
                break;
            case MemoryOperand operand:
                mod += "00";
                mem += "110";

                var memoryArgument = (MemoryOperand)Arguments[0];
                
                var variableValue = AssemblerTable
                    .Where(x => x.Command is AssemblerVariable)
                    .Where(x => memoryArgument.Value.Contains(((AssemblerVariable)x.Command).VariableName))
                    .Select(x => x.BinaryAddress)
                    .FirstOrDefault();
                
                additions += variableValue!.ExtendTo(16).ReverseTwoBytes();
                break;
            default:
                
                break;
        }
        
        code += w;
        code += mod;
        code += "010";
        code += mem;
        code += additions;


        var hexValue = code.ConvertTo(from: 2, to: 16);
        
        return new BuildResponse(offset: GetOffset(), hexValue, code);
    }

    public override int GetOffset()
    {
        return Arguments[0] is MemoryOperand ? 4 : 2;
    }
}