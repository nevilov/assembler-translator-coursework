﻿using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Extensions;

namespace AssemblerTranslator.AssemblerCommands.Commands;

public class CommandLoop : AssemblerCommand
{
    public string LabelName { get; set; }
    
    public CommandLoop(string labelName) : base(null)
    {
        LabelName = labelName;
    }

    public override BuildResponse Build()
    {
        if (AssemblerTable.Length == 0)
        {
            throw new Exception("Отстутствуют необходимые данные");
        }

        var labelAddress = AssemblerTable
            .FirstOrDefault(x => x.Command is AssemblerLabel label && label.Content == LabelName);
        if (labelAddress == null)
        {
            throw new Exception($"Метка не определена {LabelName}");
        }

        var calculatedAddress = 256 - (CurrentAddress + GetOffset() - labelAddress.Address);
        var address = calculatedAddress?
            .ConvertTo(2);
        
        var code = "11100010";
        code += address;
        var hexCode = code.ConvertTo(2, 16);
        
        return new BuildResponse(GetOffset(), hexCode, code);
    }

    public override int GetOffset()
    {
        return 2;
    }
}