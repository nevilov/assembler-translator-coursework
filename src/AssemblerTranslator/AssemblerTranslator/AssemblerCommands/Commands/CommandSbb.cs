using AssemblerTranslator.AssemblerCommands.Operators;
using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.Constants;
using AssemblerTranslator.Constants.CommandFormat;
using AssemblerTranslator.Extensions;

namespace AssemblerTranslator.AssemblerCommands.Commands;

public class CommandSbb: AssemblerCommand
{
    public CommandFormat Format { get; set; }

    public CommandSbb(IOperand[] args, CommandFormat format) : base(args)
    {
        Format = format;
    }

    public override BuildResponse Build()
    {
        var operationCode = Formats.CommandFormatForSbb[Format];

        int d = 1;
        int s = 1;
        int w = 1;
        var result = operationCode.Code;

        int mod;
        switch (Format)
        {
            case CommandFormat.AxRegisterImmediate:
                result += w;
                
                var secondArgument2 = ((DirectOperand) Arguments[1]).HexValue
                    .ConvertTo(16, 2)
                    .ExtendTo(16)
                    .ReverseTwoBytes();

                result += secondArgument2;
            break;
            case CommandFormat.RegisterImmediate:
                result += s;
                result += w;
                var firstArgument = ((RegisterOperand)Arguments[0]);
                result += firstArgument.Mod;
                result += 011;
                result += PredefinedData.RegisterCodes[firstArgument.Value.ToUpper()];
                
                var secondArgument = ((DirectOperand) Arguments[1]).HexValue
                    .ConvertTo(16, 2)
                    .ExtendTo(16)
                    .ReverseTwoBytes();

                result += secondArgument;
                break;
            case CommandFormat.AxRegisterMemory:
            case CommandFormat.MemoryAxRegister:
                result += w;
                var memoryArgumentAx = Format == CommandFormat.MemoryAxRegister 
                    ? ((MemoryOperand)Arguments[0])
                    : ((MemoryOperand)Arguments[1]);
                
                var variableValueAx = AssemblerTable
                    .Where(x => x.Command is AssemblerVariable)
                    .Where(x => ((AssemblerVariable)x.Command).VariableName == memoryArgumentAx.Value)
                    .Select(x => x.BinaryAddress)
                    .FirstOrDefault();
                
                result += variableValueAx!.ExtendTo(16).ReverseTwoBytes();
                break;
            case CommandFormat.MemoryRegister:
            case CommandFormat.RegisterMemory:
                d = Format == CommandFormat.MemoryRegister ? 0 : 1;
                
                result += d;
                result += w; 
                result += "00";
                var registerOperand = Format == CommandFormat.MemoryRegister 
                    ? ((RegisterOperand)Arguments[1])
                    : ((RegisterOperand)Arguments[0]);
                result += PredefinedData.RegisterCodes[registerOperand.Value.ToUpper()];
                int mem = 110;
                result += mem;
                
                var memoryArgument = Format == CommandFormat.MemoryRegister 
                    ? ((MemoryOperand)Arguments[0])
                    : ((MemoryOperand)Arguments[1]);
                
                var variableValue = AssemblerTable
                    .Where(x => x.Command is AssemblerVariable)
                    .Where(x => ((AssemblerVariable)x.Command).VariableName == memoryArgument.Value)
                    .Select(x => x.BinaryAddress)
                    .FirstOrDefault();
                
                result += variableValue!.ExtendTo(16).ReverseTwoBytes();
                break;
            case CommandFormat.RegisterRegister:
                var firstRegisterArg = ((RegisterOperand)Arguments[0]);
                var secondRegisterArg = ((RegisterOperand)Arguments[1]);
                d = firstRegisterArg.IsCustom ? 0 : 1;

                result += d;
                result += w;

                if (firstRegisterArg.IsCustom || secondRegisterArg.IsCustom)
                {
                    if (d == 1)
                    {
                        // ax, [bx]
                        result += secondRegisterArg.Mod;
                        result += PredefinedData.RegisterCodes[firstRegisterArg.Value.ToUpper()];
                        result += secondRegisterArg.Mem;
                    }
                    else
                    {
                        // [bx], ax
                        result += firstRegisterArg.Mod;
                        result += PredefinedData.RegisterCodes[secondRegisterArg.Value.ToUpper()];
                        result += firstRegisterArg.Mem;
                    }
                }
                else
                {

                    result += 11;
                    result += PredefinedData.RegisterCodes[firstRegisterArg.Value.ToUpper()];
                    result += PredefinedData.RegisterCodes[secondRegisterArg.Value.ToUpper()];   
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        return new BuildResponse(GetOffset(), result.ConvertTo(2, 16), result);
    }

    public override int GetOffset()
    {
        return Formats.CommandFormatForSbb[Format].Offset;
    }
}