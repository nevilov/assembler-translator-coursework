﻿using AssemblerTranslator.AssemblerCommands;
using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.AssemblerMeta;
using AssemblerTranslator.Extensions;
using AssemblerTranslator.Types;

namespace AssemblerTranslator;

public class CodeBuilder
{
    private readonly IAssemblerCommand[] _commands;
    private readonly IAssemblerMeta[] _metas;
    private readonly List<string> _errors;
    private readonly FileWorker _fileWorker;
    private readonly string[] _lines;

    public CodeBuilder(IAssemblerCommand[] commands, List<string> errorLines, IAssemblerMeta[] metas, string[] lines)
    {
        _commands = commands;
        _errors = errorLines;
        _metas = metas;
        _lines = lines;
        _fileWorker = new FileWorker();
    }

    public CodeBuilderResponse Build()
    {
        var iterator = 0;
        var listing = new List<string>(GetHeader());
        var responses = new List<BuildResponse>();

        var segment = (AssemblerSegment) _metas
            .First(x => x is AssemblerSegment);
        var segmentString = segment.InitialLines.Select(x =>
            {
                var result = string.Format(GetFormat(), iterator, 0, string.Empty, x);
                iterator++;
                return result;
            }
        );                  
        listing.AddRange(segmentString);
        
        var address = segment.StartAddress;
        var assemblerTable = PopulateAssemblerTable(address);

        foreach (var command in _commands)
        {
            iterator++;

            command.AssemblerTable = assemblerTable;
            command.CurrentAddress = address;

            var builderResponse = command.Build();
            responses.Add(builderResponse);

            var initialWord = command.InitialWord;
            var line = string.Format(GetFormat(), iterator, FormatAddress(address),
                builderResponse.MachineCode.ToUpper(), initialWord);
            listing.Add(line);

            address += builderResponse.Offset;
        }
        
        listing.Add("\n\n\n");
        listing.Add(GetObjectCode(segment, responses, address, segment.StartAddress));
        
        var content = string.Join('\n', listing.ToArray());
       // _fileWorker.WriteToFile(content);

        return new CodeBuilderResponse()
        {
            BuilderResponses = responses,
            Listing = content
        };
    }
    
    public string GetObjectCode(AssemblerSegment segment, List<BuildResponse> responses, int endAddr, int startAddr)
    {
        var header = $"H{segment.SegmentName}{segment.StartAddress}{FormatAddressWithoutExtend(endAddr - startAddr)}";
        var code = string.Empty;;
        int currentAddr = startAddr;
        foreach (var response in responses)
        {
            code += $"{FormatAddressWithoutExtend(currentAddr)}{FormatAddressWithoutExtend(response.Offset)}{response.MachineCode}";
            currentAddr += response.Offset;
        }
        var body = $"T{code}";
        var end = $"E{FormatAddressWithoutExtend(startAddr)}";
        
        return $"{header}\n{body}\n{end}";
    }

    private AssemblerTable[] PopulateAssemblerTable(int startAddress)
    {
        var properCommands = _commands.Where(x => x is AssemblerLabel or AssemblerVariable).ToList();
        var table = new List<AssemblerTable>();
        foreach (var command in _commands)
        {
            if (properCommands.Contains(command))
            {
                table.Add(command is AssemblerLabel
                    ? new AssemblerTable(startAddress + command.GetOffset(), command)
                    : new AssemblerTable(startAddress, command));
            }
            
            startAddress += command.GetOffset();
        }

        return table.ToArray();
    }


    private string CollectErrors()
    {
        var result = string.Empty;

        foreach (var error in _errors)
        {
            result += $"{error}";
            result += "\n";
        }

        return result;
    }

    private string FormatAddress(int address) => address.ConvertTo(16).ExtendTo(4);
    private string FormatAddressWithoutExtend(int address) => address.ConvertTo(16);

    private string GetFormat() => "[{0,9}] {1,9}: {2,-25}{3,-25}";

    private List<string> GetHeader()
    {
        return new List<string>()
        {
            new string('=', 64),
            "ИНСТИТУТ ИНФОРМАЦИОННЫХ ТЕХНОЛОГИЙ И КОМПЬЮТЕРНЫХ СИСТЕМ",
            "КАФЕДРА ИНФОРМАТИКА И ВЫЧИСЛИТЕЛЬНАЯ ТЕХНИКА",
            "ПРОФИЛЬ: ТЕХНОЛОГИИ ПРОГРАММИРОВАНИЯ | НАПРАВЛЕНИЕ: ПРОГРАММНАЯ ИНЖЕНЕРИЯ",
            "ВЫПОЛНИЛ: ЮСУФОВ АЛИМ СЕЙРАНОВИЧ",
            new string('=', 64),
            string.Empty,
            $"[{DateTime.UtcNow:g}]",
            string.Format(GetFormat(), "LINE", "LOC",
                "MACHINE CODE", "SOURCE"),
            new string('=', 64)
        };
    }

}