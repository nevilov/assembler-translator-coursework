using AssemblerTranslator.Types;

namespace AssemblerTranslator;

public class Runner
{
    private readonly RunnerMode _runnerMode;

    public Runner(RunnerMode runnerMode)
    {
        _runnerMode = runnerMode;
    }

    public CodeBuilderResponse Run(string value)
    {
        Parser parser = new Parser();
        switch (_runnerMode)
        {
            case RunnerMode.SourceCode:
            {
                var lines = value.Split("\n");
                parser.Process(lines);
                break;
            }
            case RunnerMode.File:
            {
                const string fileName = "file.txt";
                parser.Process(fileName);
                break;
            }
        }

        var cb = new CodeBuilder(
            parser.AssemblerCommands.ToArray(),
            parser.ErrorsList,
            parser.AssemblerMetas.ToArray(),
            parser.Lines);
        var result = cb.Build();
        return result;
    }
}

public enum RunnerMode
{
    File,
    SourceCode
}