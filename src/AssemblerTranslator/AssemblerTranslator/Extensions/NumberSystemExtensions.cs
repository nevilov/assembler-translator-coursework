using System.Text;

namespace AssemblerTranslator.Extensions;

public static class NumberSystemExtensions
{
    private const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";


    public static string ExtendTo(this string value, int endLength)
    {
        var zeroesToAdd = endLength - value.Length;
        if (zeroesToAdd == 0)
        {
            return value;
        }
        return new string('0', zeroesToAdd) + value;
    }

    public static string ReverseTwoBytes(this string value)
        => value.Substring(8, 8) + value[..8];
    
    public static string ConvertTo(this int value, int baseValue)
    {
        if (baseValue < 2 || baseValue > Digits.Length)
            throw new ArgumentException($"Invalid base: {baseValue}. Must be between 2 and {Digits.Length}.", nameof(baseValue));

        if (value == 0)
            return "0";

        StringBuilder result = new StringBuilder();
        bool isNegative = value < 0;
        if (isNegative)
            value = -value;

        while (value > 0)
        {
            int remainder = value % baseValue;
            result.Insert(0, Digits[remainder]);
            value /= baseValue;
        }

        if (isNegative)
            result.Insert(0, '-');

        return result.ToString();
    }
    

    public static string ConvertTo(this string value, int from, int to)
    {
        var intValue = Convert.ToInt32(value, from);
        return Convert.ToString(intValue,  to);
    }

    public static int ConvertFrom(this string value, int baseValue)
    {
        if (baseValue < 2 || baseValue > Digits.Length)
            throw new ArgumentException($"Invalid base: {baseValue}. Must be between 2 and {Digits.Length}.", nameof(baseValue));

        if (string.IsNullOrEmpty(value))
            return 0;

        int result = 0;
        bool isNegative = false;
        int startIndex = 0;

        if (value[0] == '-')
        {
            isNegative = true;
            startIndex = 1;
        }

        for (int i = startIndex; i < value.Length; i++)
        {
            int digit = Digits.IndexOf(value[i]);
            if (digit < 0 || digit >= baseValue)
                throw new ArgumentException($"Invalid digit '{value[i]}' for base {baseValue}.", nameof(value));

            result = result * baseValue + digit;
        }

        if (isNegative)
            result = -result;

        return result;
    }
    
    public static int ConvertToDecimal(this string number, int sourceBase)
    {
        if (sourceBase < 2 || sourceBase > 36)
            throw new ArgumentException("Invalid source base.", "sourceBase");

        int result = 0;

        foreach (char digit in number)
        {
            int value = CharToValue(digit);
            if (value >= sourceBase)
                throw new ArgumentException("Invalid digit for source base.", "number");
            result = result * sourceBase + value;
        }

        return result;
    }
    
    private static int CharToValue(char digit)
    {
        if (digit >= '0' && digit <= '9')
            return digit - '0';
        if (digit >= 'A' && digit <= 'Z')
            return digit - 'A' + 10;
        if (digit >= 'a' && digit <= 'z')
            return digit - 'a' + 10;
        throw new ArgumentException("Invalid digit.", "digit");
    }
}