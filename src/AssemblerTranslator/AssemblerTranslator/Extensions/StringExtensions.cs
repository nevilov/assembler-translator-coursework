namespace AssemblerTranslator.Extensions;

public static class StringExtensions
{
    public static string ExcludeComment(this string line) => 
        new string(line.TakeWhileCustom(x => x != ';', false).ToArray());
 
    public static string[] SplitOnTokens(this string line) => line
        .ExcludeComment()
        .Split(',', ' ')
        .Where(x => !string.IsNullOrWhiteSpace(x))
        .Select(x => x
            .Trim()
            .ToLower())
        .ToArray();
}