﻿using AssemblerTranslator.AssemblerCommands.Types;
using AssemblerTranslator.AssemblerMeta.Types;
using AssemblerTranslator.Types;

namespace AssemblerTranslator.AssemblerMeta;

public abstract class IAssemblerMeta
{

    public IAssemblerMeta()
    {
    }
    
    public abstract SegmentResponse Build();
}