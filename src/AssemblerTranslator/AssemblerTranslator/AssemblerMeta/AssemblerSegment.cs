﻿using AssemblerTranslator.AssemblerMeta.Types;


namespace AssemblerTranslator.AssemblerMeta;

public class AssemblerSegment: IAssemblerMeta
{
    public int StartAddress { get; set; }
    public string SegmentName { get; set; }
    public string[] InitialLines { get; set; }

    public AssemblerSegment(int startAddress, string segmentName, string[] initialLines)
    {
        SegmentName = segmentName;
        InitialLines = initialLines;
        StartAddress = startAddress;
    }

    public override SegmentResponse Build()
    {
        throw new NotImplementedException();
    }
}